#!/bin/bash
# Office for Mac Toolkit
#This script is on ALPHA stage and it may contain bugs and/or typos
#Script is licensed under Creative Commons Attribution-NoDerivs
# Init
FILE="/tmp/out.$$"
GREP="/bin/grep"
#....
# Make sure only sudo can run this script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as sudo!" 1>&2
   exit 1
fi
# ...
clear
PS3='Please enter your choice: '
options=("Remove Office 2011/Office 365" "Remove Office 2016/Office 365" "Download Office 2011/2016/365" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Remove Office 2011/Office 365")
		clear
		echo PLEASE MAKE BACKUPS BEFORE YOU BEGIN. THIS WILL ERASE ALL THE DATA FROM OUTLOOK!!
		read -p "Press any key to continue"
		echo "Killing Office applications"
		pkill -9 "Microsoft Word"
		pkill -9 "Microsoft Excel"
		pkill -9 "Microsoft Outlook"
		pkill -9 "Microsoft Word"
		pkill -9 "Microsoft Document Connection"
			echo "Removing Office"
			rm -rf rm -rf /Applications/Microsoft\ Office\ 2011/
			rm -rf ~/Library/Application\ Support/Microsoft
			rm -rf ~/Library/Preferences/com.microsoft.*
			rm -rf ~/Library/Preferences/ByHost/com.microsoft.*
			rm -rf /Library/LaunchDaemons/com.microsoft.*
			rm -rf /Library/Preferences/com.microsoft.*
			rm -rf ~/Documents/Microsoft\ User\ Data/
		echo Done
		echo Remove any Office icon found on Dock or Launchpad manually
		read -p "Press any key to continue"
		clear
            ;;
        "Remove Office 2016/Office 365")
		clear
		echo PLEASE MAKE BACKUPS BEFORE YOU BEGIN. THIS WILL ERASE ALL THE DATA FROM OUTLOOK!!
		read -p "Press any key to continue"
			echo "Removing Office"
			echo Still work in progress. Nothing has been removed.
		read -p "Press any key to continue"
		clear
            ;;
		"Download Office 2011/2016/365")
		clear
		PS3='Please enter your choice: '
options=("Download Danish Office 2011/365" "Download Finnish Office 2011/365" "Download Norwegian Office 2011/365" "Download Swedish Office 2011/365" "Download Office 2016/365" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Download Danish Office 2011/365")
			clear
            echo "Downloading"
			curl -O http://officecdn.microsoft.com/pr/MacOffice2011/da-dk/MicrosoftOffice2011.dmg
		echo Done
		read -p "Download Complete. Press any key to continue"
		clear
            ;;
        "Download Finnish Office 2011/365")
			clear
            echo "Downloading"
			curl -O http://officecdn.microsoft.com/pr/MacOffice2011/fi-fi/MicrosoftOffice2011.dmg
		echo Done
		read -p "Download Complete. Press any key to continue"
		clear
            ;;
		"Download Norwegian Office 2011/365")
			clear
            echo "Downloading"
			curl -O http://officecdn.microsoft.com/pr/MacOffice2011/nb-no/MicrosoftOffice2011.dmg
		echo Done
		read -p  "Download Complete. Press any key to continue"
		clear
            ;;
		"Download Swedish Office 2011/365")
			clear
            echo "Downloading"
			curl -O http://officecdn.microsoft.com/pr/MacOffice2011/sv-se/MicrosoftOffice2011.dmg
		echo Done
		read -p  "Download Complete. Press any key to continue"	
		clear		
            ;;
		"Download Office 2016/365")
			clear
            echo "Downloading"
			curl -O http://officecdn.microsoft.com.edgesuite.net/pr/C1297A47-86C4-4C1F-97FA-950631F94777/OfficeMac/Microsoft_Office_2016_15.25.0_160818_Installer32.pkg
		echo Done
		read -p  "Download Complete. Press any key to continue"	
		clear
			;;
        "Quit")
		clear
            exit
            ;;
        *) echo invalid option;;
    esac
done
;;
        "Quit")
		clear
            exit
            ;;
        *) echo invalid option;;
    esac
done 